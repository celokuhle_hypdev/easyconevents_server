const app = require("express");
const mongoose = require("mongoose");
const router = app.Router();
require("dotenv").config();
const User = require("../models/user");
const { hashPass, doPassesMatch } = require("../loginUtil");
const { generateAccessToken } = require("../jwtUtil");
const { authenticateAccessToken, hasAdminRole } = require("../jwtUtil");
const {
  BAD_REQUEST,
  UNAUTHORISED,
  INTERNAL_SERVER_ERROR,
  OK,
  SUCCESSFULLY_CREATED,
} = require("../httpStatus");

mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on("error", (err) => {
  console.log("Error connecting to DB for User schema: " + err);
});

db.once("open", () => {
  console.log("Connected to Mongo DB for User schema");
});

//post
router.post("/", checkIfUsernameIsTaken, setUserInput, async (req, res) => {
  const user = new User(req.mypayload);

  await user.save((error, result) => {
    if (error) return res.sendStatus(INTERNAL_SERVER_ERROR);

    res.status(SUCCESSFULLY_CREATED).send(result);
  });
});

router.post("/login", authenticate, (req, res) => {
  const token = generateAccessToken({
    email: req.user.email,
    isAdmin: req.user.isAdmin,
  });
  res.setHeader("authorization", token);
  res.sendStatus(OK);
});

async function authenticate(req, res, next) {
  const userName = req.body.email;
  const password = req.body.password;

  await User.findOne({ email: userName }, async (error, user) => {
    if (error) {
      return res.sendStatus(UNAUTHORISED);
    }

    if (user !== null) {
      const matches = await doPassesMatch(password, user.password);
      if (matches == true) {
        req.user = user;
        return next();
      }
      return res.sendStatus(UNAUTHORISED);
    } else {
      return res.sendStatus(UNAUTHORISED);
    }
  });
}

//find all
router.get("/", async (req, res) => {
  await User.find((error, results) => {
    if (error) {
      console.log(error);
      return res.sendStatus(INTERNAL_SERVER_ERROR);
    }

    res.json(results);
  });
});

//delete
router.delete(
  "/:id",
  authenticateAccessToken,
  hasAdminRole,
  async (req, res) => {
    const filter = { _id: req.params.id };
    await User.deleteOne(filter, (error, result) => {
      if (error) return res.sendStatus(INTERNAL_SERVER_ERROR);

      res.send(result);
    });
  }
);

async function setUserInput(req, res, next) {
  req.mypayload = {
    name: req.body.name,
    surname: req.body.surname,
    dateOfBirth: req.body.dateOfBirth,
    email: req.body.email,
    password: await hashPass(req.body.password),
    isAdmin: req.body.isAdmin,
  };
  next();
}

async function checkIfUsernameIsTaken(req, res, next) {
  await User.findOne({ email: req.body.email }, (error, user) => {
    if (error) return res.sendStatus(INTERNAL_SERVER_ERROR);

    if (user !== null) {
      res.status(BAD_REQUEST).send("Email already taken");
    } else {
      next();
    }
  });
}

module.exports = router;
