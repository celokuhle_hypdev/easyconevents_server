const app = require("express");
const mongoose = require("mongoose");
const router = app.Router();
const ConferenceEvent = require("../models/event");
require("dotenv").config();
const { authenticateAccessToken, hasAdminRole } = require("../jwtUtil");
const {
  INTERNAL_SERVER_ERROR,
  SUCCESSFULLY_CREATED,
  ACCESS_FORBIDDEN,
} = require("../httpStatus");

mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on("error", (err) => {
  console.log("Error connecting to DB for Event schema: " + err);
});

db.once("open", () => {
  console.log("Connected to Mongo DB for Event schema");
});

//get
router.get("/", authenticateAccessToken, async (req, res) => {
  await ConferenceEvent.find((error, results) => {
    if (error) {
      res.sendStatus(INTERNAL_SERVER_ERROR);
    } else {
      res.json(results);
    }
  });
});

//post
router.post(
  "/",
  authenticateAccessToken,
  hasAdminRole,
  setInput,
  async (req, res) => {
    const event = new ConferenceEvent(req.mypayload);

    await event.save((error, result) => {
      if (error) return res.sendStatus(INTERNAL_SERVER_ERROR);

      res.status(SUCCESSFULLY_CREATED).send(result);
    });
  }
);

//update
router.put(
  "/:id",
  authenticateAccessToken,
  hasAdminRole,
  setQueryFilter,
  setInput,
  async (req, res) => {
    await ConferenceEvent.updateOne(
      req.queryFilter,
      req.mypayload,
      (error, result) => {
        if (error) return res.sendStatus(INTERNAL_SERVER_ERROR);

        res.send(result);
      }
    );
  }
);

//delete
router.delete(
  "/:id",
  authenticateAccessToken,
  hasAdminRole,
  setQueryFilter,
  async (req, res) => {
    await ConferenceEvent.deleteOne(req.queryFilter, (error, result) => {
      if (error) return res.sendStatus(INTERNAL_SERVER_ERROR);

      res.send(result);
    });
  }
);

function setInput(req, res, next) {
  req.mypayload = {
    name: req.body.name,
    description: req.body.description,
    location: req.body.location,
    date: req.body.date,
    duration: req.body.duration,
    entryPrice: req.body.entryPrice,
    isActive: req.body.isActive,
  };
  next();
}

function setQueryFilter(req, res, next) {
  req.queryFilter = { _id: req.params.id };
  next();
}

module.exports = router;
