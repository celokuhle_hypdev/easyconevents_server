const { json } = require("express");
const express = require("express"); // import express from "express";
const app = express();
require("dotenv").config(); //loads contents of .env to out props
const cors = require("cors");

let event = require("./routes/event");
let user = require("./routes/user");

/**
 * exposedHeaders: Configures the Access-Control-Expose-Headers CORS header.
 */
app.use(
  cors({ origin: process.env.CORS_ORIGIN, exposedHeaders: "authorization" })
);

/** request body was not being passed until this was imported.
  json() returns a request json passer middleware.
  Since this is at the global level, this middleware will be used/called
  before any of my ReST action methods 
 */
app.use(json());

app.use("/event", event);
app.use("/user", user);

app.listen(process.env.PORT, () => {
  console.log(`Listening on port ${process.env.PORT}`);
});
