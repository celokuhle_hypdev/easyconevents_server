const bcrypt = require("bcrypt");

/**
 * The password is hashed so that even if the database is comprised, it's not easy to
 * know the exact passwords. Decrypting the passwords would take hours, if at all possible.
 *
 * The salt is used to ensure that even if the users use the same password, 
 * the saved encrypted passwords are not the same because the salt would not be the same. 
 * During a DB compromise, if one password is decrypted, all other similar passwords would not be
 *  compromised as well, because of the salt.
 *
 * -genSalt() defaults to rounds of 10 if no rounds are provided.
 *  --the higher the rounds, the  longer bcrypt takes to generate the salt
 *
 * - shorthand for automatic salt generation: bcrypt.hash(password, 10);
 * 
 * Promise example:
  await bcrypt.hash(password, salt).then((value) => {
    hash = value;
  });
  return hash;
 *
 */
async function hashPass(password) {
  let salt = await bcrypt.genSalt();
  return await bcrypt.hash(password, salt);
}

/**
 * when brcrypt hashes the password, the result hash string contains the salt aswell.
 * bcrpyt.compare uses this salt, which is part of the saved hashed password string, to hash the verify 
 * password and use the result in a comparison with the saved hash string from the database.
 *
 * @param {*} verifyPassword
 * @param {*} savedPassword
 */
async function doPassesMatch(verifyPassword, savedPassword) {
  const decoded = bcrypt.compare(verifyPassword, savedPassword);
  return decoded;
}

module.exports = { hashPass, doPassesMatch };
