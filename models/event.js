const mongoose = require("mongoose");

const eventSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: String,
  location: { type: String, required: true },
  date: { type: Date, required: true },
  duration: String,
  entryPrice: { type: Number, min: 0 },
  isActive: { type: Boolean, default: true },
});

const initialCollectionName = "event"; //the final collectionName in the DB will be, {initialCollectionName} pluralized

let Event = mongoose.model(initialCollectionName, eventSchema); //return a model which can be used to access the DB and perfom the relevant CRUD operations

module.exports = Event;
// similar to: export default Account;
