const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: { type: String, required: true },
  surname: String,
  dateOfBirth: { type: Date, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  isAdmin: { type: Boolean, required: true },
});

const initialCollectionName = "user";

const User = mongoose.model(initialCollectionName, userSchema);

module.exports = User;