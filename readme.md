## How to use the app
* You can access this API [here](https://easy-con-events.herokuapp.com/).
* You can make ReST calls to the API above

## Test and run this app on local machine
* Run 'npm install' to install all dependencies listed on the package.json file.
* You can run API tests on .REST files if you are using Visual Studio Code Rest extension
* To modify the the mongoDB URL you can modify a property on the .env file.
* JSON secret token is also stored as a .env property.

## Security on the app
* Users are required to login, and their passwords are stored in encypted form in the database.
* Most of the resources require a JSON web tokens to access them, ontop of login.
* JWT token authentication are generated and verified before granting access to certain parts.

## 3rd party libraries
* dotenv - to access configuration properties stored in the .env file.
* nodemon - dev dependency to auto restart my node server on every file change and save.
* cors - CrossOrigin Resource Sharing for setting which client domains are allowed to access resources on this server 
* bcrypt - to generate salts, hash passwords, and compare passwords from users against encrypted passwords from the    database.
* jwt - JsonWebToken, used as an authentication mechanism to generate tokens and verify them when the client sends them back.
* mongodb - used as an ORM library to access the db and create more structured db models.
    * I also practised accessing the DB using the mongodb native library
    

## App deployment:
* The app is deployed on heroku, and is deployed seperately from the front end.
    * link above.
* The reason is to allow multiple clients to this backend server as a ReST API.
* The database is hosted on mongodb atlas cloud 
