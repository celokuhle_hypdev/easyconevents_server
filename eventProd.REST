GET https://easy-con-events.herokuapp.com/event/
authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNlbG9AbXllemEuY29tIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjEzMjExMjk0LCJleHAiOjE2MTMyMTMwOTR9.barYTsZg7UkA_QLJZxDbZFXN5NpAgJUiuOVUjomCsCA


###

POST https://easy-con-events.herokuapp.com/event/
content-type: application/json
authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNlbG9AbXllemEuY29tIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjEzMjExMjk0LCJleHAiOjE2MTMyMTMwOTR9.barYTsZg7UkA_QLJZxDbZFXN5NpAgJUiuOVUjomCsCA

{
   "name": "5km fun run",
  "description": "Come and run with us",
  "location": "Randpark Ridge",
  "date": "2021-02-25",
  "duration": "07:00am - 16:00pm",
  "entryPrice": "150",
  "isActive": "true"
}

###

PUT https://easy-con-events.herokuapp.com/event/601781083e4fd148f891b340
authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNlbG9AbXllemEuY29tIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjEzMjExMjk0LCJleHAiOjE2MTMyMTMwOTR9.barYTsZg7UkA_QLJZxDbZFXN5NpAgJUiuOVUjomCsCA
content-type: application/json

{
  "name": "new 5km fun run",
  "description": "Come and run with us now",
  "location": "Randpark Ridge North",
  "date": "2021-03-25",
  "duration": "07:00am - 17:00pm",
  "entryPrice": "2000",
  "isActive": "false"
}


###

DELETE https://easy-con-events.herokuapp.com/event/60176acc5364744ba844b3d5
authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNlbG9AbXllemEuY29tIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjEzMjExMjk0LCJleHAiOjE2MTMyMTMwOTR9.barYTsZg7UkA_QLJZxDbZFXN5NpAgJUiuOVUjomCsCA

