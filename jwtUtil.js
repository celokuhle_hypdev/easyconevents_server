const jwt = require("jsonwebtoken");
const { ACCESS_DENIED } = require("./httpStatus");

function generateAccessToken(user) {
  return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: "30m" });
}

function authenticateAccessToken(req, res, next) {
  let token = req.headers["authorization"];
  if (token == null) return res.sendStatus(ACCESS_DENIED);

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodedUser) => {
    if (err) return res.sendStatus(ACCESS_DENIED);

    req.user = decodedUser;

    next();
  });
}

function hasAdminRole(req, res, next) {
  if (req.user.isAdmin !== true) return res.sendStatus(ACCESS_DENIED);
  next();
}
module.exports = { generateAccessToken, authenticateAccessToken, hasAdminRole };
